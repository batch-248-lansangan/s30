db.fruits.insertMany([
            {
                name : "Apple",
                color : "Red",
                stock : 20,
                price: 40,
                supplier_id : 1,
                onSale : true,
                origin: [ "Philippines", "US" ]
            },

            {
                name : "Banana",
                color : "Yellow",
                stock : 15,
                price: 20,
                supplier_id : 2,
                onSale : true,
                origin: [ "Philippines", "Ecuador" ]
            },

            {
                name : "Kiwi",
                color : "Green",
                stock : 25,
                price: 50,
                supplier_id : 1,
                onSale : true,
                origin: [ "US", "China" ]
            },

            {
                name : "Mango",
                color : "Yellow",
                stock : 10,
                price: 120,
                supplier_id : 2,
                onSale : false,
                origin: [ "Philippines", "India" ]
            }
        ]);

db.fruits.find({ price: {$lte: 50} );

	//MongoDB Aggregation

		//an aggregate is a cluster of things that have come or have been brought together
		//In MongoDB, aggregation operations group values from multiple documents together

		db.fruits.aggregate([


				{$match: {onSale: true}},
				{$group: {_id: "$supplier_id", total: {$sum: "$stock"} } }
			]);

		// Aggregation Pipeline Stages
			//Aggregation is typically done in 2-3 steps. Each process in aggregation is called a stage
		/*

			$match - is ued to match or get document which SATISFIES the condition
			//syntax - {$match: {field:<value>}};

			$group - allows us to group together documents and create an analysis out of the grouped documents

			_id: in the group stage, essentially associates an id to our results
			_id: also, it detemermines the number of groups

		*/

		//Field projection with aggregation

			db.fruits.aggregate([


				{$match: {onSale: true}},
				{$group: {_id: "$supplier_id", total: {$sum: "$stock"} } },
				{$project: {_id: 0}}
			]);

			/*
				the $project can be used when aggregating data to include/exclude fields from the returned results
				{$project: field: 1/0}}

				1 shows up
				0 does not show up
			*/


			//sort aggregated results
			db.fruits.aggregate([


				{$match: {onSale: true}},
				{$group: {_id: "$supplier_id", total: {$sum: "$stock"} } },
				{$sort: {total: -1}}
			]);

			/*

				$sort can be used to change the order of aggregated results

				Syntax:
				{$sort{field: 1/-1}}

				1 - ascending
				-1 - descending
			*/

			//aggregating results based on array fields

			db.fruits.aggregate([

			{$unwind: "$origin"}
				]);
		/*
			$unwind deconstructs an array field from field with our array value to output a results for each elements

			Syntax:

			{$unwind: field}

		*/

			db.fruits.aggregate([

			{$unwind: "$origin"},
			{group:{_id" $origin", kinds: {$sum : 1}}}

		]);			
			
		//MiniA1
				
		db.fruits.aggregate([
    		{ $match: {color: "Yellow" } },
      		{ $count: "yellowFruits" }
      	]);

		db.fruits.aggregate([
    		{ $match: {color: {$regex: "yElloW", $options: '$i' } } },
      		{ $count: "yellowFruits" }
      	]);

		//MiniA2

		db.fruits.aggregate([
    		{ $match: {stock: {$lte: 10 } }},
      		{ $count: "fruitStocks" }
      	]);

		//MiniA3 find 

   	db.fruits.aggregate([

			{ $match: { onSale: true }},
			{ $group : { _id: "$supplier_id", max_stock: { $max: "$stock"}}}			
  		]);

