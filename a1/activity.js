/*1. Create an activity.js file on where to write and save the solution for the activity.

2. Note: use {$match: {onSale: true}}*/

//A. Use the count operator to count the total number of fruits with stock more than 20.

		//A
			db.fruits.aggregate([
    		{ $match: {stock: {$gte: 20 } }},
      		{ $count: "enoughStock" }
      		]);
//B. Use the average operator to get the average price of fruits onSale per supplier.

       	 db.fruits.aggregate(
  		 [ { $match: { onSale: true }},
                {$sort: {total: -1}} ,
    	 {
       		$group:
         {
           _id: "$supplier_id",
           avg_price: { $avg: { $sum: [ "$onSale", "$price" ]  } },
         }
     }
   ]
);

//C. Use the max operator to get the highest price of a fruit per supplier.

			   	db.fruits.aggregate([

			{ $match: { onSale: true }},
                         {$sort: {total: -1}} ,
			{ $group : { _id: "$supplier_id", max_stock: { $max: "$price"}}}			
  		]);

//D. Use the min operator to get the lowest price of a fruit per supplier.
			   				   	db.fruits.aggregate([

		db.fruits.aggregate([

			{ $match: { onSale: true }},
                         {$sort: {total: -1}} ,
			{ $group : { _id: "$supplier_id", min_price: { $min: "$price"}}}			
  		]);


/*3. Create a git repository named s30.
4. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
5. Add the link in Boodle.
6. Send a screencap of only the result
*/



    //B 		